<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdenCompra extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ordenescompra';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['otro_servicio','precio_otro_servicio','precio_total',
    'tasa_interes','estatus_pago'];
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = ['remember_token','id_usuario','id_ordencompra','id_proyecto','id_pago'];



    public function usuarios()
    {
        return $this->hasOne('App\User', 'id_usuario', 'id_ordencompra');
    }

    public function pagos()
    {
        return $this->hasMany('App\Pago', 'id_pago', 'id_ordencompra');
    }

    public function proyectos()
    {
        return $this->hasOne('App\Proyecto', 'id_proyecto', 'id_ordencompra');
    }

    public function servicios()
    {
        return $this->belongsToMany('App\Servicio', 'ordenescompra_servicios','id_servicio','id_ordencompra');
    }
}
