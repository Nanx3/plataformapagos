<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bitacoras';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['archivo', 'fecha', 'nombre'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = ['remember_token','id_usuario','id_bitacora'];


    public function usuarios()
    {
        return $this->hasOne('App\User', 'id_usuario', 'id_bitacora');
    }

}
