<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contratos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['representante_legal', 'imagen', 'fecha_inicial','fecha_final', 'cantidad'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = ['remember_token','id_usuario','id_proyecto','id_contrato'];




    public function usuarios()
    {
        return $this->hasOne('App\User', 'id_usuario', 'id_contrato');
    }

    public function proyectos()
    {
        return $this->hasOne('App\Proyecto', 'id_proyecto', 'id_contrato');
    }
}
