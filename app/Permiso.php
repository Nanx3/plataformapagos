<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
    ];
    protected $table = 'permisos';

    protected $hidden = ['remember_token'];


    public function roles()
    {
        return $this->belongsToMany('App\Rol', 'roles_permisos','id_permiso','id_rol');
    }

}
