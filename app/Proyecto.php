<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'proyectos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre', 'descripcion', 'fecha_inicial','fecha_final', 'coordinador','estatus'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = ['remember_token','id_usuario','id_contrato','id_proyecto', 'id_ordencompra'];



    public function usuarios()
    {
        return $this->hasOne('App\User', 'id_usuario', 'id_proyecto');
    }

    public function contratos()
    {
        return $this->hasMany('App\Contrato', 'id_contrato', 'id_proyecto');
    }

    public function ordenescompra()
    {
        return $this->hasMany('App\OrdenCompra', 'id_ordencompra', 'id_proyecto');
    }


}
