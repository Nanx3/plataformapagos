<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
    ];
    protected $table = 'roles';

    protected $hidden = ['remember_token'];


    public function permisos()
    {
        return $this->belongsToMany('App\Permiso', 'roles_permisos','id_rol','id_permiso');
    }
}
