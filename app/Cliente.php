<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clientes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'telefono_opcional1','telefono_opcional2',
        'saldo','contacto_nombre','contacto_telefono',
        'contacto_telefono_opcional1','contacto_telefono_opcional2','contacto_email','rfc',
        'domicilio_fiscal','cp','razon_social'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = ['id_usuario','remember_token'];

    public function usuarios()
    {
        return $this->belongsTo('App\User','id_usuario');
    }

}
