<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'servicios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','precio','descripcion'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = ['remember_token','id_ordencompra','id_servicio'];


    public function ordenescompra()
    {
        return $this->belongsToMany('App\OrdenCompra', 'ordenescompra_servicios','id_ordencompra','id_servicio');
    }

}
