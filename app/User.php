<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'email', 'password','telefono','tipo'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = [
        'password', 'id_usuario', 'id_rol', 'id_contrato', 'id_proyecto',
        'id_ordencompra','id_pago', 'remember_token'];



    public function roles()
    {
        return $this->belongsToMany('App\Rol','usuario_rol','id_usuario','id_rol');
    }

    public function contratos()
    {
        return $this->hasMany('App\Contrato', 'id_contrato', 'id_usuario');
    }

    public function proyectos()
    {
        return $this->hasMany('App\Proyecto', 'id_proyecto', 'id_usuario');
    }

    public function ordenescompra()
    {
        return $this->hasMany('App\OrdenCompra', 'id_ordencompra', 'id_usuario');
    }

    public function bitacoras()
    {
        return $this->hasMany('App\Bitacora', 'id_bitacora', 'id_usuario');
    }

    public function clientes()
    {
        return $this->hasOne('App\Cliente');
    }
}
