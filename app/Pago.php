<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pagos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['fecha_pago', 'cantidad', 'imagen','estatus_subido','estatus_verificado'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = ['remember_token','id_usuario','id_ordencompra','id_pago'];


    
    public function ordenescompra()
    {
        return $this->hasOne('App\OrdenCompra', 'id_ordencompra', 'id_pago');
    }

}
