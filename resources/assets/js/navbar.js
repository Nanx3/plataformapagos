/**
 * Created by SOFTAM02 on 7/4/16.
 */

import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


class navbarComponent extends React.Component{
    //Método de pintado
    render () {
        //Tag obligatorio para mostrar elemento
        return (
            <MuiThemeProvider>
                <headerHTML />
            </MuiThemeProvider>
        );
    }
}
export default navbarComponent;


class  navbarHTML extends React.Component {

    //constructor
    constructor(props) {
        super(props);
    }

    //Método para dibujar
    render () {

        return (

        <div id="menubar" className="menubar-inverse ">
            <div className="menubar-fixed-panel">
                <div>
                    <a className="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                        <i className="fa fa-bars"></i>
                    </a>
                </div>
                <div className="expanded">
                    <a href="">
                        <span className="text-lg text-bold text-primary ">DOOD</span>
                    </a>
                </div>
            </div>
            <div className="menubar-scroll-panel">

                <ul id="main-menu" className="gui-controls">
                    <li>
                        <a href="" className="active">
                            <div className="gui-icon"><i className="md md-home"></i></div>
                            <span className="title">Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="" className="active">
                            <div className="gui-icon"><i className="md md-home"></i></div>
                            <span className="title">Proyectos</span>
                        </a>
                    </li>
                    <li>
                        <a href="" className="active">
                            <div className="gui-icon"><i className="md md-home"></i></div>
                            <span className="title">Clientes</span>
                        </a>
                    </li>
                    <li>
                        <a href="" className="active">
                            <div className="gui-icon"><i className="md md-home"></i></div>
                            <span className="title">Contratos</span>
                        </a>
                    </li>
                    <li>
                        <a href="" className="active">
                            <div className="gui-icon"><i className="md md-home"></i></div>
                            <span className="title">Ordenes compra</span>
                        </a>
                    </li>
                    <li>
                        <a href="" className="active">
                            <div className="gui-icon"><i className="md md-home"></i></div>
                            <span className="title">Pagos</span>
                        </a>
                    </li>
                </ul>

                <div className="menubar-foot-panel">
                    <small className="no-linebreak hidden-folded">
                        <span className="opacity-75">Copyright &copy; 2016</span> <strong>DOOD</strong>
                    </small>
                </div>
            </div>
        </div>

        );
    }
}

