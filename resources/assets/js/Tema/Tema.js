
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import {greenA700,cyan500, cyan700,
    pinkA200,
    grey100, grey300, grey400, grey500,
    white, darkBlack, fullBlack,} from 'material-ui/styles/colors';


const temaPersonalizado = getMuiTheme({


    palette: {
        primary1Color: greenA700
    }

});

export default temaPersonalizado;