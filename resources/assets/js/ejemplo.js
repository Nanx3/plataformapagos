
/*Bibliotecas*/
var  React = require('react');
var  ReactDOM = require('react-dom');
var  ReactRouter = require('react-router');
var  ReactBootstrap = require ('react-bootstrap');
var  Remarkable = require('remarkable');
var $ = require("jquery");


var Modal = ReactBootstrap.Modal;
var Button = ReactBootstrap.Button;
var ButtonToolbar = ReactBootstrap.ButtonToolbar;
var FormGroup= ReactBootstrap.FormGroup;
var ControlLabel= ReactBootstrap.ControlLabel;
var FormControl= ReactBootstrap.FormControl;
var HelpBlock= ReactBootstrap.HelpBlock;

/*EJEMPLO*/
const FormExample = React.createClass({
/* Definición de variables */
    getInitialState() {
        return {
            value: '',
            paises : [],
            pais: null,
            padrinosSelect:null,
            paiseees:null,
        };
    },

/*Metodo GET con REACT*/
    getPaises(){
        $.ajax({
            type: 'GET',
            url: 'ejemplo/paises',
            context: this,
            success: function (data) {
                data = JSON.parse(data);
                console.log(data);
                if (data[0]) {
                    this.setState({
                        paises : data,
                 });
                }
            }.bind(this),
            error: function (error) {
                console.log(error);
            }.bind(this)
        });
    },

/*Metodo POST con REACT*/
    crearPaises(){

     // Obtener token
        var token = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            type: 'POST',
            url: 'crear/paises',
            context: this,
            data: {
                value: this.state.value,
                _token:token
            },
            success: function (data) {
                data = JSON.parse(data);

                if (data[0]) {
                    for (var i = 0; i < data.length; i++) {
                        paisesSelect.push({value: data[i].id, label: nombre});
                    }
                    paiseees = data;
                }
            }.bind(this),
            error: function (error) {
                console.log(error);
            }.bind(this)
        });
    },


    componentDidMount: function() {
        this.getPaises();
    },

    getValidationState() {
        const length = this.state.value.length;
        if (length > 10) return 'success';
        else if (length > 5) return 'warning';
        else if (length > 0) return 'error';
    },

    handleChange(e) {
        this.setState({ value: e.target.value });
    },

    render() {
        return (
            <form >
                <FormGroup
                    controlId="formBasicText"
                    validationState={this.getValidationState()} >
                    <ControlLabel>Working example with validation</ControlLabel>
                    <FormControl
                        type="text"
                        value={this.state.value}
                        placeholder="Enter text"
                        onChange={this.handleChange}/>
                    <FormControl.Feedback />
                    <HelpBlock>Validation is based on string length.</HelpBlock>

                    <Button type="submit" onClick={this.crearPaises}>Enviar</Button>

                </FormGroup>
            </form>
        );
    }
});


export default FormExample;


var MySmallModal = React.createClass({
    render() {
        return (
            <Modal {...this.props} bsSize="small" aria-labelledby="contained-modal-title-sm">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">Modal heading</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <FormExample />

                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.props.onHide}>Close</Button>
                </Modal.Footer>
            </Modal>
        );
    }
});

var MyLargeModal = React.createClass({
    render() {
        return (
            <Modal {...this.props} bsSize="large" aria-labelledby="contained-modal-title-lg">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-lg">Modal heading</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h4>Wrapped Text</h4>
                    <p>Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                </Modal.Body>
                <Modal.Footer>

                    <Button onClick={this.props.onHide}>Close</Button>

                </Modal.Footer>
            </Modal>
        );
    }
});

var BotonModal = React.createClass({

    getInitialState() {
        return {
            smShow: false, lgShow: false
        };
    },

    render() {
        let smClose = () => this.setState({ smShow: false });
        let lgClose = () => this.setState({ lgShow: false });
        return (
            <ButtonToolbar>
                <Button bsStyle="primary" onClick={()=>this.setState({ smShow: true })}>
                    Launch small demo modal
                </Button>

                <Button bsStyle="primary" onClick={()=>this.setState({ lgShow: true })}>
                    Launch large demo modal
                </Button>

                <MySmallModal show={this.state.smShow} onHide={smClose} />

                <MyLargeModal show={this.state.lgShow} onHide={lgClose} />
            </ButtonToolbar>
        );
    }
});


export default BotonModal;

