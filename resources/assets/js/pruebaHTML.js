
/*Bibliotecas*/
var  React = require('react');
var  ReactDOM = require('react-dom');
var  ReactRouter = require('react-router');
var  ReactBootstrap = require ('react-bootstrap');
var  Remarkable = require('remarkable');
var $ = require("jquery");

/*EJEMPLO*/
const pruebaHTML = React.createClass({

render() {
    return (


<div>
        <header id="header" >
            <div class="headerbar">
                <div class="headerbar-left">
                    <ul class="header-nav header-nav-options">
                        <li class="header-nav-brand" >
                            <div class="brand-holder">
                                <a href="../../html/dashboards/dashboard.html">
                                    <span class="text-lg text-bold text-primary">MATERIAL ADMIN</span>
                                </a>
                            </div>
                        </li>
                        <li>
                            <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                                <i class="fa fa-bars"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="headerbar-right">
                    <ul class="header-nav header-nav-options">
                        <li>
                            <form class="navbar-search" role="search">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="headerSearch" placeholder="Enter your keyword"></input>
                                </div>
                                <button type="submit" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-search"></i></button>
                            </form>
                        </li>
                        <li class="dropdown hidden-xs">
                            <a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
                                <i class="fa fa-bell"></i><sup class="badge style-danger">4</sup>
                            </a>
                            <ul class="dropdown-menu animation-expand">
                                <li class="dropdown-header">Today's messages</li>
                                <li>
                                    <a class="alert alert-callout alert-warning" href="javascript:void(0);">
                                        <img class="pull-right img-circle dropdown-avatar" src="../../assets/img/avatar2.jpg?1404026449" alt="" />
                                        <strong>Alex Anistor</strong><br/>
                                        <small>Testing functionality...</small>
                                    </a>
                                </li>
                                <li>
                                    <a class="alert alert-callout alert-info" href="javascript:void(0);">
                                        <img class="pull-right img-circle dropdown-avatar" src="../../assets/img/avatar3.jpg?1404026799" alt="" />
                                        <strong>Alicia Adell</strong><br/>
                                        <small>Reviewing last changes...</small>
                                    </a>
                                </li>
                                <li class="dropdown-header">Options</li>
                                <li><a href="../../html/pages/login.html">View all messages <span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>
                                <li><a href="../../html/pages/login.html">Mark as read <span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>
                            </ul>
                        </li>
                        <li class="dropdown hidden-xs">
                            <a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
                                <i class="fa fa-area-chart"></i>
                            </a>
                            <ul class="dropdown-menu animation-expand">
                                <li class="dropdown-header">Server load</li>
                                <li class="dropdown-progress">
                                    <a href="javascript:void(0);">
                                        <div class="dropdown-label">
                                            <span class="text-light">Server load <strong>Today</strong></span>
                                            <strong class="pull-right">93%</strong>
                                        </div>
                                        <div class="progress"><div class="progress-bar progress-bar-danger" style="width: 93%"></div></div>
                                    </a>
                                </li>
                                <li class="dropdown-progress">
                                    <a href="javascript:void(0);">
                                        <div class="dropdown-label">
                                            <span class="text-light">Server load <strong>Yesterday</strong></span>
                                            <strong class="pull-right">30%</strong>
                                        </div>
                                        <div class="progress"><div class="progress-bar progress-bar-success" style="width: 30%"></div></div>
                                    </a>
                                </li>
                                <li class="dropdown-progress">
                                    <a href="javascript:void(0);">
                                        <div class="dropdown-label">
                                            <span class="text-light">Server load <strong>Lastweek</strong></span>
                                            <strong class="pull-right">74%</strong>
                                        </div>
                                        <div class="progress"><div class="progress-bar progress-bar-warning" style="width: 74%"></div></div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="header-nav header-nav-profile">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
                                <img src="../../assets/img/avatar1.jpg?1403934956" alt="" />
								<span class="profile-info">
									Daniel Johnson
									<small>Administrator</small>
								</span>
                            </a>
                            <ul class="dropdown-menu animation-dock">
                                <li class="dropdown-header">Config</li>
                                <li><a href="../../html/pages/profile.html">My profile</a></li>
                                <li><a href="../../html/pages/blog/post.html">My blog <span class="badge style-danger pull-right">16</span></a></li>
                                <li><a href="../../html/pages/calendar.html">My appointments</a></li>
                                <li class="divider"></li>
                                <li><a href="../../html/pages/locked.html"><i class="fa fa-fw fa-lock"></i> Lock</a></li>
                                <li><a href="../../html/pages/login.html"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="header-nav header-nav-toggle">
                        <li>
                            <a class="btn btn-icon-toggle btn-default" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false">
                                <i class="fa fa-ellipsis-v"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>




                <div id="base">
                    <div className="offcanvas">
                    </div>
                    <div id="content">
                        <section>
                            <div className="section-body">
                                <div className="row">

                                    <div className="col-md-3 col-sm-6">
                                        <div className="card">
                                            <div className="card-body no-padding">
                                                <div className="alert alert-callout alert-info no-margin">
                                                    <strong className="pull-right text-success text-lg">0,38% <i className="md md-trending-up"></i></strong>
                                                    <strong className="text-xl">$ 32,829</strong><br/>
                                                    <span className="opacity-50">Revenue</span>
                                                    <div className="stick-bottom-left-right">
                                                        <div className="height-2 sparkline-revenue" data-line-color="#bdc1c1"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="col-md-3 col-sm-6">
                                        <div className="card">
                                            <div className="card-body no-padding">
                                                <div className="alert alert-callout alert-warning no-margin">
                                                    <strong className="pull-right text-warning text-lg">0,01% <i className="md md-swap-vert"></i></strong>
                                                    <strong className="text-xl">432,901</strong><br/>
                                                    <span className="opacity-50">Visits</span>
                                                    <div className="stick-bottom-right">
                                                        <div className="height-1 sparkline-visits" data-bar-color="#e5e6e6"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-3 col-sm-6">
                                        <div className="card">
                                            <div className="card-body no-padding">
                                                <div className="alert alert-callout alert-danger no-margin">
                                                    <strong className="pull-right text-danger text-lg">0,18% <i className="md md-trending-down"></i></strong>
                                                    <strong className="text-xl">42.90%</strong><br/>
                                                    <span className="opacity-50">Bounce rate</span>
                                                    <div className="stick-bottom-left-right">
                                                        <div className="progress progress-hairline no-margin">
                                                            <div className="progress-bar progress-bar-danger" style="width:43%"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-3 col-sm-6">
                                        <div className="card">
                                            <div className="card-body no-padding">
                                                <div className="alert alert-callout alert-success no-margin">
                                                    <h1 className="pull-right text-success"><i className="md md-timer"></i></h1>
                                                    <strong className="text-xl">54 sec.</strong><br/>
                                                    <span className="opacity-50">Avg. time on site</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">

                                    <div className="col-md-9">
                                        <div className="card ">
                                            <div className="row">
                                                <div className="col-md-8">
                                                    <div className="card-head">
                                                        <header>Site activity</header>
                                                    </div>
                                                    <div className="card-body height-8">
                                                        <div id="flot-visitors-legend" className="flot-legend-horizontal stick-top-right no-y-padding"></div>
                                                        <div id="flot-visitors" className="flot height-7" data-title="Activity entry" data-color="#7dd8d2,#0aa89e"></div>
                                                    </div>
                                                </div>
                                                <div className="col-md-4">
                                                    <div className="card-head">
                                                        <header>Today's stats</header>
                                                    </div>
                                                    <div className="card-body height-8">
                                                        <strong>214</strong> members
                                                        <span className="pull-right text-success text-sm">0,18% <i className="md md-trending-up"></i></span>
                                                        <div className="progress progress-hairline">
                                                            <div className="progress-bar progress-bar-primary-dark" style="width:43%"></div>
                                                        </div>
                                                        756 pageviews
                                                        <span className="pull-right text-success text-sm">0,68% <i className="md md-trending-up"></i></span>
                                                        <div className="progress progress-hairline">
                                                            <div className="progress-bar progress-bar-primary-dark" style="width:11%"></div>
                                                        </div>
                                                        291 bounce rates
                                                        <span className="pull-right text-danger text-sm">21,08% <i className="md md-trending-down"></i></span>
                                                        <div className="progress progress-hairline">
                                                            <div className="progress-bar progress-bar-danger" style="width:93%"></div>
                                                        </div>
                                                        32,301 visits
                                                        <span className="pull-right text-success text-sm">0,18% <i className="md md-trending-up"></i></span>
                                                        <div className="progress progress-hairline">
                                                            <div className="progress-bar progress-bar-primary-dark" style="width:63%"></div>
                                                        </div>
                                                        132 pages
                                                        <span className="pull-right text-success text-sm">0,18% <i className="md md-trending-up"></i></span>
                                                        <div className="progress progress-hairline">
                                                            <div className="progress-bar progress-bar-primary-dark" style="width:47%"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-3">
                                        <div className="card">
                                            <div className="card-head">
                                                <header className="text-primary">Server status</header>
                                            </div>
                                            <div className="card-body height-4">
                                                <div className="pull-right text-center">
                                                    <em className="text-primary">Temperature</em>
                                                    <br></br>
                                                    <div id="serverStatusKnob" className="knob knob-shadow knob-primary knob-body-track size-2">
                                                        <input type="text" className="dial" data-min="0" data-max="100" data-thickness=".09" value="50" data-readOnly="true" ></input>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body height-4 no-padding">
                                                <div className="stick-bottom-left-right">
                                                    <div id="rickshawGraph" className="height-4" data-color1="#0aa89e" data-color2="rgba(79, 89, 89, 0.2)"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div className="row">

                                    <div className="col-md-3">
                                        <div className="card ">
                                            <div className="card-head">
                                                <header>Todo's</header>
                                                <div className="tools">
                                                    <a className="btn btn-icon-toggle btn-close"><i className="md md-close"></i></a>
                                                </div>
                                            </div>
                                            <div className="card-body no-padding height-9 scroll">
                                                <ul className="list" data-sortable="true">
                                                    <li className="tile">
                                                        <div className="checkbox checkbox-styled tile-text">
                                                            <label>
                                                                <input type="checkbox" checked></input>
                                                                    <span>Call clients for follow-up</span>
                                                            </label>
                                                        </div>
                                                        <a className="btn btn-flat ink-reaction btn-default">
                                                            <i className="md md-delete"></i>
                                                        </a>
                                                    </li>
                                                    <li className="tile">
                                                        <div className="checkbox checkbox-styled tile-text">
                                                            <label>
                                                                <input type="checkbox"></input>
														<span>
															Schedule meeting
															<small>opportunity for new customers</small>
														</span>
                                                            </label>
                                                        </div>
                                                        <a className="btn btn-flat ink-reaction btn-default">
                                                            <i className="md md-delete"></i>
                                                        </a>
                                                    </li>
                                                    <li className="tile">
                                                        <div className="checkbox checkbox-styled tile-text">
                                                            <label>
                                                                <input type="checkbox"></input>
														<span>
															Upload files to server
															<small>The files must be shared with all members of the board</small>
														</span>
                                                            </label>
                                                        </div>
                                                        <a className="btn btn-flat ink-reaction btn-default">
                                                            <i className="md md-delete"></i>
                                                        </a>
                                                    </li>
                                                    <li className="tile">
                                                        <div className="checkbox checkbox-styled tile-text">
                                                            <label>
                                                                <input type="checkbox">
                                                                    <span>Forward important tasks</span></input>
                                                            </label>
                                                        </div>
                                                        <a className="btn btn-flat ink-reaction btn-default">
                                                            <i className="md md-delete"></i>
                                                        </a>
                                                    </li>
                                                    <li className="tile">
                                                        <div className="checkbox checkbox-styled tile-text">
                                                            <label>
                                                                <input type="checkbox"></input>
                                                                    <span>Forward important tasks</span>
                                                            </label>
                                                        </div>
                                                        <a className="btn btn-flat ink-reaction btn-default">
                                                            <i className="md md-delete"></i>
                                                        </a>
                                                    </li>
                                                    <li className="tile">
                                                        <div className="checkbox checkbox-styled tile-text">
                                                            <label>
                                                                <input type="checkbox">
                                                                    <span>Forward important tasks</span></input>
                                                            </label>
                                                        </div>
                                                        <a className="btn btn-flat ink-reaction btn-default">
                                                            <i className="md md-delete"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-6">
                                        <div className="card">
                                            <div className="card-head">
                                                <header>Registration history</header>
                                                <div className="tools">
                                                    <a className="btn btn-icon-toggle btn-refresh"><i className="md md-refresh"></i></a>
                                                    <a className="btn btn-icon-toggle btn-collapse"><i className="fa fa-angle-down"></i></a>
                                                    <a className="btn btn-icon-toggle btn-close"><i className="md md-close"></i></a>
                                                </div>
                                            </div>
                                            <div className="card-body no-padding height-9">
                                                <div className="row">
                                                    <div className="col-sm-6 hidden-xs">
                                                        <div className="force-padding text-sm text-default-light">
                                                            <p>
                                                                <i className="md md-mode-comment text-primary-light"></i>
                                                                The registrations are measured from the time that the redesign was fully implemented and after the first e-mailing.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-6">
                                                        <div className="force-padding pull-right text-default-light">
                                                            <h2 className="no-margin text-primary-dark"><span className="text-xxl">66.05%</span></h2>
                                                            <i className="fa fa-caret-up text-success fa-fw"></i> more registrations
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="stick-bottom-left-right force-padding">
                                                    <div id="flot-registrations" className="flot height-5" data-title="Registration history" data-color="#0aa89e"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-3">
                                        <div className="card">
                                            <div className="card-head">
                                                <header>New registrations</header>
                                                <div className="tools hidden-md">
                                                    <a className="btn btn-icon-toggle btn-close"><i className="md md-close"></i></a>
                                                </div>
                                            </div>
                                            <div className="card-body no-padding height-9 scroll">
                                                <ul className="list divider-full-bleed">
                                                    <li className="tile">
                                                        <div className="tile-content">
                                                            <div className="tile-icon">
                                                                <img src="assets/img/avatar9.jpg?1404026744" alt="" />
                                                            </div>
                                                            <div className="tile-text">Ann Laurens</div>
                                                        </div>
                                                        <a className="btn btn-flat ink-reaction">
                                                            <i className="md md-block text-default-light"></i>
                                                        </a>
                                                    </li>
                                                    <li className="tile">
                                                        <div className="tile-content">
                                                            <div className="tile-icon">
                                                                <img src="assets/img/avatar4.jpg?1404026791" alt="" />
                                                            </div>
                                                            <div className="tile-text">Alex Nelson</div>
                                                        </div>
                                                        <a className="btn btn-flat ink-reaction">
                                                            <i className="md md-block text-default-light"></i>
                                                        </a>
                                                    </li>
                                                    <li className="tile">
                                                        <div className="tile-content">
                                                            <div className="tile-icon">
                                                                <img src="assets/img/avatar11.jpg?1404026774" alt="" />
                                                            </div>
                                                            <div className="tile-text">Mary Peterson</div>
                                                        </div>
                                                        <a className="btn btn-flat ink-reaction">
                                                            <i className="md md-block text-default-light"></i>
                                                        </a>
                                                    </li>
                                                    <li className="tile">
                                                        <div className="tile-content">
                                                            <div className="tile-icon">
                                                                <img src="assets/img/avatar7.jpg?1404026721" alt="" />
                                                            </div>
                                                            <div className="tile-text">Philip Ericsson</div>
                                                        </div>
                                                        <a className="btn btn-flat ink-reaction">
                                                            <i className="md md-block text-default-light"></i>
                                                        </a>
                                                    </li>
                                                    <li className="tile">
                                                        <div className="tile-content">
                                                            <div className="tile-icon">
                                                                <img src="assets/img/avatar8.jpg?1404026729" alt="" />
                                                            </div>
                                                            <div className="tile-text">Jim Peters</div>
                                                        </div>
                                                        <a className="btn btn-flat ink-reaction">
                                                            <i className="md md-block text-default-light"></i>
                                                        </a>
                                                    </li>
                                                    <li className="tile">
                                                        <div className="tile-content">
                                                            <div className="tile-icon">
                                                                <img src="assets/img/avatar2.jpg?1404026449" alt="" />
                                                            </div>
                                                            <div className="tile-text">Jessica Cruise</div>
                                                        </div>
                                                        <a className="btn btn-flat ink-reaction">
                                                            <i className="md md-block text-default-light"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>
                    </div>

                    <div id="menubar" className="menubar-inverse ">
                        <div className="menubar-fixed-panel">
                            <div>
                                <a className="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                                    <i className="fa fa-bars"></i>
                                </a>
                            </div>
                            <div className="expanded">
                                <a href="html/dashboards/dashboard.html">
                                    <span className="text-lg text-bold text-primary ">MATERIAL&nbsp;ADMIN</span>
                                </a>
                            </div>
                        </div>
                        <div className="menubar-scroll-panel">


                            <ul id="main-menu" className="gui-controls">

                                <li>
                                    <a href="html/dashboards/dashboard.html" className="active">
                                        <div className="gui-icon"><i className="md md-home"></i></div>
                                        <span className="title">Dashboard</span>
                                    </a>
                                </li>
                                <li className="gui-folder">
                                    <a>
                                        <div className="gui-icon"><i className="md md-email"></i></div>
                                        <span className="title">Email</span>
                                    </a>
                                    start submenu
                                    <ul>
                                        <li><a href="html/mail/inbox.html" ><span className="title">Inbox</span></a></li>
                                        <li><a href="html/mail/compose.html" ><span className="title">Compose</span></a></li>
                                        <li><a href="html/mail/reply.html" ><span className="title">Reply</span></a></li>
                                        <li><a href="html/mail/message.html" ><span className="title">View message</span></a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="html/layouts/builder.html" >
                                        <div className="gui-icon"><i className="md md-web"></i></div>
                                        <span className="title">Layouts</span>
                                    </a>
                                </li>

                                <li className="gui-folder">
                                    <a>
                                        <div className="gui-icon"><i className="fa fa-puzzle-piece fa-fw"></i></div>
                                        <span className="title">UI elements</span>
                                    </a>

                                    <ul>
                                        <li><a href="html/ui/colors.html" ><span className="title">Colors</span></a></li>
                                        <li><a href="html/ui/typography.html" ><span className="title">Typography</span></a></li>
                                        <li><a href="html/ui/cards.html" ><span className="title">Cards</span></a></li>
                                        <li><a href="html/ui/buttons.html" ><span className="title">Buttons</span></a></li>
                                        <li><a href="html/ui/lists.html" ><span className="title">Lists</span></a></li>
                                        <li><a href="html/ui/tabs.html" ><span className="title">Tabs</span></a></li>
                                        <li><a href="html/ui/accordions.html" ><span className="title">Accordions</span></a></li>
                                        <li><a href="html/ui/messages.html" ><span className="title">Messages</span></a></li>
                                        <li><a href="html/ui/offcanvas.html" ><span className="title">Off-canvas</span></a></li>
                                        <li><a href="html/ui/grid.html" ><span className="title">Grid</span></a></li>
                                        <li className="gui-folder">
                                            <a href="javascript:void(0);">
                                                <span className="title">Icons</span>
                                            </a>

                                            <ul>
                                                <li><a href="html/ui/icons/materialicons.html" ><span className="title">Material Design Icons</span></a></li>
                                                <li><a href="html/ui/icons/fontawesome.html" ><span className="title">Font Awesome</span></a></li>
                                                <li><a href="html/ui/icons/glyphicons.html" ><span className="title">Glyphicons</span></a></li>
                                                <li><a href="html/ui/icons/skycons.html" ><span className="title">Skycons</span></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <li className="gui-folder">
                                    <a>
                                        <div className="gui-icon"><i className="fa fa-table"></i></div>
                                        <span className="title">Tables</span>
                                    </a>

                                    <ul>
                                        <li><a href="html/tables/static.html" ><span className="title">Static Tables</span></a></li>
                                        <li><a href="html/tables/dynamic.html" ><span className="title">Dynamic Tables</span></a></li>
                                        <li><a href="html/tables/responsive.html" ><span className="title">Responsive Table</span></a></li>
                                    </ul>
                                </li>

                                <li className="gui-folder">
                                    <a>
                                        <div className="gui-icon"><span className="glyphicon glyphicon-list-alt"></span></div>
                                        <span className="title">Forms</span>
                                    </a>

                                    <ul>
                                        <li><a href="html/forms/basic.html" ><span className="title">Form basic</span></a></li>
                                        <li><a href="html/forms/advanced.html" ><span className="title">Form advanced</span></a></li>
                                        <li><a href="html/forms/layouts.html" ><span className="title">Form layouts</span></a></li>
                                        <li><a href="html/forms/editors.html" ><span className="title">Editors</span></a></li>
                                        <li><a href="html/forms/validation.html" ><span className="title">Form validation</span></a></li>
                                        <li><a href="html/forms/wizard.html" ><span className="title">Form wizard</span></a></li>
                                    </ul>
                                </li>

                                <li className="gui-folder">
                                    <a>
                                        <div className="gui-icon"><i className="md md-computer"></i></div>
                                        <span className="title">Pages</span>
                                    </a>

                                    <ul>
                                        <li className="gui-folder">
                                            <a href="javascript:void(0);">
                                                <span className="title">Contacts</span>
                                            </a>
                                            <ul>
                                                <li><a href="html/pages/contacts/search.html" ><span className="title">Search</span></a></li>
                                                <li><a href="html/pages/contacts/details.html" ><span className="title">Contact card</span></a></li>
                                                <li><a href="html/pages/contacts/add.html" ><span className="title">Insert contact</span></a></li>
                                            </ul>

                                        <li className="gui-folder">
                                            <a href="javascript:void(0);">
                                                <span className="title">Search</span>
                                            </a>
                                            <ul>
                                                <li><a href="html/pages/search/results-text.html" ><span className="title">Results - Text</span></a></li>
                                                <li><a href="html/pages/search/results-text-image.html" ><span className="title">Results - Text and Image</span></a></li>
                                            </ul>
                                        </li>
                                        <li className="gui-folder">
                                            <a href="javascript:void(0);">
                                                <span className="title">Blog</span>
                                            </a>
                                            <ul>
                                                <li><a href="html/pages/blog/masonry.html" ><span className="title">Blog masonry</span></a></li>
                                                <li><a href="html/pages/blog/list.html" ><span className="title">Blog list</span></a></li>
                                                <li><a href="html/pages/blog/post.html" ><span className="title">Blog post</span></a></li>
                                            </ul>
                                        </li>
                                        <li className="gui-folder">
                                            <a href="javascript:void(0);">
                                                <span className="title">Error pages</span>
                                            </a>
                                            <ul>
                                                <li><a href="html/pages/404.html" ><span className="title">404 page</span></a></li>
                                                <li><a href="html/pages/500.html" ><span className="title">500 page</span></a></li>
                                            </ul>
                                        </li>
                                </li>
                                <li>
                                    <a href="html/charts/charts.html" >
                                        <div className="gui-icon"><i className="md md-assessment"></i></div>
                                        <span className="title">Charts</span>
                                    </a>
                                </li>

                                <li className="gui-folder">
                                    <a>
                                        <div className="gui-icon"><i className="fa fa-folder-open fa-fw"></i></div>
                                        <span className="title">Menu levels demo</span>
                                    </a>

                                    <ul>
                                        <li><a href="#"><span className="title">Item 1</span></a></li>
                                        <li><a href="#"><span className="title">Item 1</span></a></li>
                                        <li className="gui-folder">
                                            <a href="javascript:void(0);">
                                                <span className="title">Open level 2</span>
                                            </a>

                                            <ul>
                                                <li><a href="#"><span className="title">Item 2</span></a></li>
                                                <li className="gui-folder">
                                                    <a href="javascript:void(0);">
                                                        <span className="title">Open level 3</span>
                                                    </a>
                                                    <ul>
                                                        <li><a href="#"><span className="title">Item 3</span></a></li>
                                                        <li><a href="#"><span className="title">Item 3</span></a></li>
                                                        <li className="gui-folder">
                                                            <a href="javascript:void(0);">
                                                                <span className="title">Open level 4</span>
                                                            </a>
                                                            <ul>
                                                                <li><a href="#"><span className="title">Item 4</span></a></li>
                                                                <li className="gui-folder">
                                                                    <a href="javascript:void(0);">
                                                                        <span className="title">Open level 5</span>
                                                                    </a>
                                                                    <ul>
                                                                        <li><a href="#"><span className="title">Item 5</span></a></li>
                                                                        <li><a href="#"><span className="title">Item 5</span></a></li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                            </ul>
                                </li>
                            </ul>
                            <div className="menubar-foot-panel">
                                <small className="no-linebreak hidden-folded">
                                    <span className="opacity-75">Copyright &copy; 2014</span> <strong>CodeCovers</strong>
                                </small>
                            </div>
                        </div>
                    </div>
                    <div className="offcanvas">

                        <div id="offcanvas-search" className="offcanvas-pane width-8">
                            <div className="offcanvas-head">
                                <header className="text-primary">Search</header>
                                <div className="offcanvas-tools">
                                    <a className="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                                        <i className="md md-close"></i>
                                    </a>
                                </div>
                            </div>
                            <div className="offcanvas-body no-padding">
                                <ul className="list ">
                                    <li className="tile divider-full-bleed">
                                        <div className="tile-content">
                                            <div className="tile-text"><strong>A</strong></div>
                                        </div>
                                    </li>
                                    <li className="tile">
                                        <a className="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                            <div className="tile-icon">
                                                <img src="assets/img/avatar4.jpg?1404026791" alt="" />
                                            </div>
                                            <div className="tile-text">
                                                Alex Nelson
                                                <small>123-123-3210</small>
                                            </div>
                                        </a>
                                    </li>
                                    <li className="tile">
                                        <a className="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                            <div className="tile-icon">
                                                <img src="assets/img/avatar9.jpg?1404026744" alt="" />
                                            </div>
                                            <div className="tile-text">
                                                Ann Laurens
                                                <small>123-123-3210</small>
                                            </div>
                                        </a>
                                    </li>
                                    <li className="tile divider-full-bleed">
                                        <div className="tile-content">
                                            <div className="tile-text"><strong>J</strong></div>
                                        </div>
                                    </li>
                                    <li className="tile">
                                        <a className="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                            <div className="tile-icon">
                                                <img src="assets/img/avatar2.jpg?1404026449" alt="" />
                                            </div>
                                            <div className="tile-text">
                                                Jessica Cruise
                                                <small>123-123-3210</small>
                                            </div>
                                        </a>
                                    </li>
                                    <li className="tile">
                                        <a className="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                            <div className="tile-icon">
                                                <img src="assets/img/avatar8.jpg?1404026729" alt="" />
                                            </div>
                                            <div className="tile-text">
                                                Jim Peters
                                                <small>123-123-3210</small>
                                            </div>
                                        </a>
                                    </li>
                                    <li className="tile divider-full-bleed">
                                        <div className="tile-content">
                                            <div className="tile-text"><strong>M</strong></div>
                                        </div>
                                    </li>
                                    <li className="tile">
                                        <a className="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                            <div className="tile-icon">
                                                <img src="assets/img/avatar5.jpg?1404026513" alt="" />
                                            </div>
                                            <div className="tile-text">
                                                Mabel Logan
                                                <small>123-123-3210</small>
                                            </div>
                                        </a>
                                    </li>
                                    <li className="tile">
                                        <a className="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                            <div className="tile-icon">
                                                <img src="assets/img/avatar11.jpg?1404026774" alt="" />
                                            </div>
                                            <div className="tile-text">
                                                Mary Peterson
                                                <small>123-123-3210</small>
                                            </div>
                                        </a>
                                    </li>
                                    <li className="tile">
                                        <a className="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                            <div className="tile-icon">
                                                <img src="assets/img/avatar3.jpg?1404026799" alt="" />
                                            </div>
                                            <div className="tile-text">
                                                Mike Alba
                                                <small>123-123-3210</small>
                                            </div>
                                        </a>
                                    </li>
                                    <li className="tile divider-full-bleed">
                                        <div className="tile-content">
                                            <div className="tile-text"><strong>N</strong></div>
                                        </div>
                                    </li>
                                    <li className="tile">
                                        <a className="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                            <div className="tile-icon">
                                                <img src="assets/img/avatar6.jpg?1404026572" alt="" />
                                            </div>
                                            <div className="tile-text">
                                                Nathan Peterson
                                                <small>123-123-3210</small>
                                            </div>
                                        </a>
                                    </li>
                                    <li className="tile divider-full-bleed">
                                        <div className="tile-content">
                                            <div className="tile-text"><strong>P</strong></div>
                                        </div>
                                    </li>
                                    <li className="tile">
                                        <a className="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                            <div className="tile-icon">
                                                <img src="assets/img/avatar7.jpg?1404026721" alt="" />
                                            </div>
                                            <div className="tile-text">
                                                Philip Ericsson
                                                <small>123-123-3210</small>
                                            </div>
                                        </a>
                                    </li>
                                    <li className="tile divider-full-bleed">
                                        <div className="tile-content">
                                            <div className="tile-text"><strong>S</strong></div>
                                        </div>
                                    </li>
                                    <li className="tile">
                                        <a className="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
                                            <div className="tile-icon">
                                                <img src="assets/img/avatar10.jpg?1404026762" alt="" />
                                            </div>
                                            <div className="tile-text">
                                                Samuel Parsons
                                                <small>123-123-3210</small>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div id="offcanvas-chat" className="offcanvas-pane style-default-light width-12">
                            <div className="offcanvas-head style-default-bright">
                                <header className="text-primary">Chat with Ann Laurens</header>
                                <div className="offcanvas-tools">
                                    <a className="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                                        <i className="md md-close"></i>
                                    </a>
                                    <a className="btn btn-icon-toggle btn-default-light pull-right" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false">
                                        <i className="md md-arrow-back"></i>
                                    </a>
                                </div>
                                <form className="form">
                                    <div className="form-group floating-label">
                                        <textarea name="sidebarChatMessage" id="sidebarChatMessage" className="form-control autosize" rows="1"></textarea>
                                        <label for="sidebarChatMessage">Leave a message</label>
                                    </div>
                                </form>
                            </div>
                            <div className="offcanvas-body">
                                <ul className="list-chats">
                                    <li>
                                        <div className="chat">
                                            <div className="chat-avatar"><img className="img-circle" src="assets/img/avatar1.jpg?1403934956" alt="" /></div>
                                            <div className="chat-body">
                                                Yes, it is indeed very beautiful.
                                                <small>10:03 pm</small>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="chat-left">
                                        <div className="chat">
                                            <div className="chat-avatar"><img className="img-circle" src="assets/img/avatar9.jpg?1404026744" alt="" /></div>
                                            <div className="chat-body">
                                                Did you see the changes?
                                                <small>10:02 pm</small>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="chat">
                                            <div className="chat-avatar"><img className="img-circle" src="assets/img/avatar1.jpg?1403934956" alt="" /></div>
                                            <div className="chat-body">
                                                I just arrived at work, it was quite busy.
                                                <small>06:44pm</small>
                                            </div>
                                            <div className="chat-body">
                                                I will take look in a minute.
                                                <small>06:45pm</small>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="chat-left">
                                        <div className="chat">
                                            <div className="chat-avatar"><img className="img-circle" src="assets/img/avatar9.jpg?1404026744" alt="" /></div>
                                            <div className="chat-body">
                                                The colors are much better now.
                                            </div>
                                            <div className="chat-body">
                                                The colors are brighter than before.
                                                I have already sent an example.
                                                This will make it look sharper.
                                                <small>Mon</small>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="chat">
                                            <div className="chat-avatar"><img className="img-circle" src="assets/img/avatar1.jpg?1403934956" alt="" /></div>
                                            <div className="chat-body">
                                                Are the colors of the logo already adapted?
                                                <small>Last week</small>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
        );
    }
});

export default pruebaHTML;
