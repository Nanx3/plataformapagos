/**
 * Created by SOFTAM02 on 7/4/16.
 */

import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


class headerComponent extends React.Component{
    //Método de pintado
    render () {
        //Tag obligatorio para mostrar elemento
        return (
            <MuiThemeProvider>
                <headerHTML />
            </MuiThemeProvider>
        );
    }
}
export default headerComponent;


class  headerHTML extends React.Component {

    //constructor
    constructor(props) {
        super(props);
    }

    //Método para dibujar
    render () {

        return (
            <header id="header" >
                <div class="headerbar">
                    <div className="headerbar-left">
                        <ul className="header-nav header-nav-options">
                            <li className="header-nav-brand" >
                                <div className="brand-holder">
                                    <a href="html/dashboards/dashboard.html">
                                        <span className="text-lg text-bold text-primary">DOOD</span>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <a className="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                                    <i className="fa fa-bars"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="headerbar-right">
                        <ul className="header-nav header-nav-options">
                            <li>
                                <form className="navbar-search" role="search">
                                    <div className="form-group">
                                        <input type="text" className="form-control" name="headerSearch" placeholder="Buscar..."></input>
                                    </div>
                                    <button type="submit" className="btn btn-icon-toggle ink-reaction"><i className="fa fa-search"></i></button>
                                </form>
                            </li>
                        </ul>
                        <ul className="header-nav header-nav-profile">
                            <li className="dropdown">
                                <a href="javascript:void(0);" className="dropdown-toggle ink-reaction" data-toggle="dropdown">
                                    <img src="img/avatar1.jpg" alt="Usuario" />
								<span className="profile-info">
									Daniel Johnson
									<small>Administrator</small>
								</span>
                                </a>
                                <ul className="dropdown-menu animation-dock">
                                    <li className="dropdown-header">Ver</li>
                                    <li><a href="">Mi perfil</a></li>
                                    <li className="divider"></li>
                                    <li><a href=""><i className="fa fa-fw fa-power-off text-danger"></i> Salir</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul className="header-nav header-nav-toggle">
                            <li>
                                <a className="btn btn-icon-toggle btn-default" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false">
                                    <i className="fa fa-ellipsis-v"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </header>
        );
    }
}

