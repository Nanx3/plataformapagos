
//Librerias por default
import   React from 'react';
import   ReactDOM from 'react-dom';

//Componentes

import   ModalCompletoEncabezadoAgregar  from './EncabezadosYAgregarTabla';
import   ModalAgregar from './formaAgregar';
import   headerComponent from './header';



// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();


 ReactDOM.render(<ModalCompletoEncabezadoAgregar
        etiquetas={ {
    Componente:"Sector",
    Modulo:"Preparación",
    Ruta:"sector/preparacion/crear"
    } } />
    , document.getElementById('app'));



/*
ReactDOM.render(<headerComponent/>
 , document.getElementById('app'));

*/