/**
 * Created by saulzini on 6/29/16.
 */
//Defaults
import   React from 'react';
import   ReactRouter from 'react-router';

//Boton
import FloatingActionButton  from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';

//Agregar Modal
import FormaModal from './formaAgregar';

//Tema Personalizado
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import temaPersonalizado from './Tema/Tema';


//Clase para crear encabezados y el boton de agregar
class EncabezadosYAgregarTabla extends  React.Component{



    constructor(props) {
        super(props);

        //definición de variables
        this.state = {open: false};


        //Se necesita hacer un bind para detectar los métodos y poderlos llamar de mejor manera
        this.handleOpen= this.handleOpen.bind(this);
        this.handleClose= this.handleClose.bind(this);

    }

    //Método para abrir y cerrar el modal
    handleOpen () {
        this.setState({open: true});
    }

    handleClose()  {
        this.setState({open: false});
    }


    adaptarEtiquetas(){

        // Componente para el nombre de sector ,inv o inv plantula
        // Modulo para el nombre de actividad agronómica preparación , fertilización, etc
        // Ruta para la ruta

        return (
            <div>
                <div className="row col-md-12" >
                    <ol className="breadcrumb bc-3" >
                        <li>
                            <a>{this.props.etiquetas["Componente"]}</a>
                        </li>
                        <li className="active">
                            <strong>{this.props.etiquetas["Modulo"]}</strong>
                        </li>
                    </ol>
                </div>

                <h2>{this.props.etiquetas["Modulo"]}</h2>

                <div className="row col-md-12">
                    <a className="pull-right" >
                        <FloatingActionButton  onTouchTap={this.handleOpen} tooltip="Añadir">

                            <ContentAdd/>

                        </FloatingActionButton>
                    </a>

                    <FormaModal state = {this.state} handleClose={this.handleClose}  />

                </div>
                <br></br>

                direccion = {this.props.etiquetas["Ruta"]}
            </div>
        )
    }

    render() {
        return(
            this.adaptarEtiquetas()
        );
    }
}

class ModalCompletoEncabezadoAgregar extends React.Component{

    //Método necesario para dibujar
    render () {


        return (
            //tiene que ir encerrado obligatoriamente por muithemeprovider
            <MuiThemeProvider muiTheme={temaPersonalizado}>
                <EncabezadosYAgregarTabla  {...this.props} />
            </MuiThemeProvider>
        );
    }
}

export default ModalCompletoEncabezadoAgregar;