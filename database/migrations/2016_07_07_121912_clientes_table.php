<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->integer('id_usuario')->unsigned()->index();
            //$table->foreign('id_usuario')->references('id')->on('users');
            $table->string('telefono_opcional1');
            $table->string('telefono_opcional2');
            $table->double('saldo');
            $table->string('contacto_nombre');
            $table->string('contacto_telefono');
            $table->string('contacto_telefono_opcional1');
            $table->string('contacto_telefono_opcional2');
            $table->string('contacto_email');
            $table->string('rfc');
            $table->string('domicilio_fiscal');
            $table->integer('cp');
            $table->string('razon_social');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clientes');
    }
}
