<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('representante_legal');
            $table->string('imagen');
            $table->date('fecha_inicial');
            $table->date('fecha_final');
            $table->double('tasa_interes');

            $table->integer('id_usuario')->unsigned();
            // $table->foreign('id_usuario')->references('id_usuario')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contratos');
    }
}
