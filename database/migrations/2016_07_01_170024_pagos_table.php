<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_pago');
            $table->smallInteger('cantidad');
            $table->string('imagen');
            $table->boolean('estatus_subido');
            $table->boolean('estatus_verificado');

            $table->integer('id_ordencompra')->unsigned();
//            $table->foreign('id_ordencompra')->references('id_ordencompra')->on('ordenescompra');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pagos');
    }
}
