<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RolesPermisosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles_permisos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_permiso')->unsigned()->index();
            $table->foreign('id_permiso')->references('id')->on('permisos')->onDelete('cascade');
            $table->integer('id_rol')->unsigned()->index();
            $table->foreign('id_rol')->references('id')->on('roles')->onDelete('cascade');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles_permisos');
    }
}
