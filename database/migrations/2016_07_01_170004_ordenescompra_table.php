<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdenescompraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordenescompra', function (Blueprint $table) {
            $table->increments('id');
            $table->string('otro_servicio');
            $table->double('precio_otro_servicio');
            $table->double('precio_total');
            $table->double('tasa_interes');
            $table->double('estatus_pago');

            $table->integer('id_usuario')->unsigned();
            // $table->foreign('id_usuario')->references('id_usuario')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ordenescompra');
    }
}
