<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdenescompraServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordenescompra_servicios', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_servicio')->unsigned()->index();
            $table->foreign('id_servicio')->references('id')->on('servicios')->onDelete('cascade');
            $table->integer('id_ordencompra')->unsigned()->index();
            $table->foreign('id_ordencompra')->references('id')->on('ordenescompra')->onDelete('cascade');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ordenescompra_servicios');
    }
}
