<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Rol_Permiso::class, function (Faker\Generator $faker) {

    $permiso= DB::table('permisos')->lists('id');
    $rol= DB::table('roles')->lists('id');
    return [
        'id_permiso' => $faker->randomElement($permiso),
        'id_rol' => $faker->randomElement($rol)
    ];
    
});