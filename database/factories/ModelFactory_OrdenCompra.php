<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\OrdenCompra::class, function (Faker\Generator $faker) {

    $usuario =  DB::table('users')->lists('id');
    
    return [
        'otro_servicio'=> $faker->sentence($nbWords = 6, $variableNbWords = true),
        'precio_otro_servicio' => $faker->randomFloat,
        'precio_total' => $faker->randomFloat,
        'tasa_interes' => $faker->randomElement([10, 20, 30]),
        'estatus_pago' => $faker->randomFloat,
        'id_usuario'=> $faker->randomElement($usuario)
    ];
});



