<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Pago::class, function (Faker\Generator $faker) {

    $ordencompra =  DB::table('ordenescompra')->lists('id');
    
    return [
        'fecha_pago' => $faker->date,
        'cantidad' => $faker->randomFloat,
        'imagen' => $faker->image,
        'estatus_subido' => $faker->boolean,
        'id_ordencompra'=> $faker->randomElement($ordencompra),
    ];
});


