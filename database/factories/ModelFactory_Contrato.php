<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Contrato::class, function (Faker\Generator $faker) {

    $usuario =  DB::table('users')->lists('id');

    return [
        'representante_legal' => $faker->name,
        'imagen' => $faker->image,
        'fecha_inicial' => $faker->date,
        'fecha_final' => $faker->date,
        'tasa_interes' => $faker->randomFloat,
        'id_usuario'=> $faker->randomElement($usuario)

    ];
});
