<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Proyecto::class, function (Faker\Generator $faker) {

    $usuario =  DB::table('users')->lists('id');

    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->text(200),
        'fecha_inicial' => $faker->date(),
        'fecha_final' => $faker->date(),
        'coordinador' => $faker->name,
        'estatus' => $faker->randomElement(['Vigente','Pausado','Cancelado']),
        'id_usuario'=> $faker->randomElement($usuario)
    ];
});
