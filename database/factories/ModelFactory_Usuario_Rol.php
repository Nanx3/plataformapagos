<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Usuario_Rol::class, function (Faker\Generator $faker) {

    $usuario= DB::table('users')->lists('id');
    $rol= DB::table('roles')->lists('id');
    return [
        'id_usuario' => $faker->randomElement($usuario),
        'id_rol' => $faker->randomElement($rol)
    ];

});
