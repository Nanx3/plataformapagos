<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\OrdenCompra_Servicio::class, function (Faker\Generator $faker) {

    $ordencompra= DB::table('ordenescompra')->lists('id');
    $servicio= DB::table('servicios')->lists('id');
    return [
        'id_ordencompra' => $faker->randomElement($ordencompra),
        'id_servicio' => $faker->randomElement($servicio)
    ];
});
