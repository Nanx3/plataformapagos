<?php

use Illuminate\Database\Seeder;

class OrdenCompra_ServicioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\OrdenCompra_Servicio::class,20)->create();
    }
}
