<?php

use Illuminate\Database\Seeder;

class OrdenCompraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\OrdenCompra::class,20)->create();
    }
}
