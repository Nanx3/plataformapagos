<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('UsuarioSeeder');
        $this->call('ClienteSeeder');
        $this->call('OrdenCompraSeeder');
        $this->call('PermisoSeeder');
        $this->call('RolSeeder');
        $this->call('PagoSeeder');
        $this->call('ProyectoSeeder');
        $this->call('ContratoSeeder');
        $this->call('BitacoraSeeder');
        $this->call('ServicioSeeder');
        $this->call('Rol_PermisoSeeder');
        $this->call('Usuario_RolSeeder');
        $this->call('OrdenCompra_ServicioSeeder');

        Model::reguard();
    }
}


