<?php

use Illuminate\Database\Seeder;

class ClienteSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuarios = DB::table('users')->where('tipo', 'Cliente')->get();
        $faker = Faker\Factory::create();
        foreach ($usuarios as $usuario) {
            DB::table('clientes')->insert([
                'id_usuario' => $usuario->id,
                'telefono_opcional1' => $faker->phoneNumber,
                'telefono_opcional2' => $faker->phoneNumber,
                'saldo' => $faker->randomFloat,
                'contacto_nombre' => $faker->name,
                'contacto_telefono' => $faker->phoneNumber,
                'contacto_telefono_opcional1' => $faker->phoneNumber,
                'contacto_telefono_opcional2' => $faker->phoneNumber,
                'contacto_email' => $faker->safeEmail,
                'rfc'=>$faker->swiftBicNumber,
                'domicilio_fiscal' => $faker->address,
                'cp'=>$faker->postcode,
                'razon_social'=>$faker->catchPhrase,
            ]);
        }
    }
}
